export const errors = {
  TOO_SHORT: "Not enough characters",
  TOO_LONG: "Too many characters",
  BAD_CHARACTERS:
    'You can use only uppercase or lowercase letters, numbers and "-" or "_"',
};

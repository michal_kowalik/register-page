import React from "react";
import {
  Card,
  Container,
  CardBody,
  FormGroup,
  Label,
  Input,
  Button,
  Alert,
} from "reactstrap";
import { CARD_NUMBER } from "../../const/routes";
import { errors } from "../../const/errors";

const IDDLE_TIME = 20;

const initialState = {
  nickname: "",
  error: "",
  isNicknameValid: false,
  countdown: IDDLE_TIME,
  countdownActive: false,
};

class RegisterPage extends React.Component {
  state = initialState;

  inactivityCountdown;

  componentWillUnmount() {
    clearInterval(this.inactivityCountdown);
  }

  startTimer = () => {
    this.setState({ countdown: IDDLE_TIME });
    const { countdownActive } = this.state;
    if (!countdownActive) {
      this.inactivityCountdown = setInterval(() => {
        if (this.state.countdown === 1) {
          this.setState(initialState);
        }
        this.setState({ countdown: this.state.countdown - 1 });
      }, 1000);
    }
    this.setState({ countdownActive: true });
  };

  validateInput = () => {
    const { nickname } = this.state;
    const nicknameRegex = /^[A-z0-9_-]+$/;
    this.setState({ error: "" });

    let isValid = true;

    if (!nicknameRegex.test(nickname)) {
      this.setState({ error: "BAD_CHARACTERS" });
      isValid = false;
    }
    if (nickname.length < 3) {
      this.setState({ error: "TOO_SHORT" });
      isValid = false;
    }
    if (nickname.length > 20) {
      this.setState({ error: "TOO_LONG" });
      isValid = false;
    }

    return isValid;
  };

  handleSubmit = (e) => {
    e.preventDefault();
    if (this.validateInput()) {
      this.props.history.push(CARD_NUMBER, { nickname: this.state.nickname });
    }
  };

  onFieldChange = (e) => {
    const { id, value } = e.target;
    this.setState({ [id]: value }, this.validateInput);
    this.startTimer();
  };

  render() {
    const { error, nickname, countdownActive, countdown } = this.state;
    return (
      <div className="section">
        <Container>
          <h1 className="title">Type your nickname</h1>
          <Card>
            <CardBody>
              {error && <Alert color="danger">{errors[error]}</Alert>}
              <form>
                <FormGroup>
                  <Label for="nickname">Nickname</Label>
                  {countdownActive && (
                    <Label className="float-right">
                      Remaining time: {countdown}
                    </Label>
                  )}
                  <Input
                    onChange={this.onFieldChange}
                    type="text"
                    value={nickname}
                    id="nickname"
                    placeholder="Enter nickname"
                  />
                </FormGroup>
                <Button
                  className="float-right"
                  color="primary"
                  type="submit"
                  onClick={this.handleSubmit}
                  disabled={!!this.state.error}
                >
                  Next
                </Button>
              </form>
            </CardBody>
          </Card>
        </Container>
      </div>
    );
  }
}

export default RegisterPage;

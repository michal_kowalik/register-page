import React from "react";
import {
  Alert,
  Button,
  Card,
  CardBody,
  Container,
  FormGroup,
  Input,
  Label,
  Modal,
  ModalBody,
} from "reactstrap";
import { errors } from "../../../const/errors";
import { REGISTER } from "../../../const/routes";

const IDDLE_TIME = 20;

class CardNumberPage extends React.Component {
  state = {
    modalVisible: false,
    cardNumber: "",
    error: "",
    countdown: IDDLE_TIME,
    countdownActive: false,
  };

  goBackCountdown;
  inactivityCountdown;

  componentWillUnmount() {
    clearTimeout(this.goBackCountdown);
    clearTimeout(this.inactivityCountdown);
  }

  startTimer = () => {
    this.setState({ countdown: IDDLE_TIME });
    const { countdownActive } = this.state;
    if (!countdownActive) {
      this.inactivityCountdown = setInterval(() => {
        if (this.state.countdown === 1) {
          clearTimeout(this.inactivityCountdown);
          this.props.history.push(REGISTER);
        }
        this.setState({ countdown: this.state.countdown - 1 });
      }, 1000);
    }
    this.setState({ countdownActive: true });
  };

  toggleModalAndMoveToRegister = () => {
    this.setState(
      {
        modalVisible: !this.state.modalVisible,
      },
      () => {
        this.goBackCountdown = setTimeout(
          () => this.props.history.push(REGISTER),
          3000
        );
      }
    );
  };

  validateInput = () => {
    const { cardNumber } = this.state;
    let isValid = true;
    this.setState({ error: "" });

    if (cardNumber.length < 10) {
      this.setState({ error: "TOO_SHORT" });
      isValid = false;
    }
    if (cardNumber.length > 10) {
      this.setState({ error: "TOO_LONG" });
      isValid = false;
    }

    return isValid;
  };

  handleSubmit = (e) => {
    e.preventDefault();
    if (this.validateInput()) {
      this.toggleModalAndMoveToRegister();
    }
  };

  onFieldChange = (e) => {
    const { id, value } = e.target;
    if (!isNaN(value)) this.setState({ [id]: value }, this.validateInput);
    this.startTimer();
  };

  render() {
    const { error, cardNumber, countdown, countdownActive } = this.state;
    return (
      <div className="section">
        <Container>
          <h1 className="title">Type your card’s number</h1>
          <Card>
            <CardBody>
              {error && <Alert color="danger">{errors[error]}</Alert>}
              <form>
                <FormGroup>
                  <Label for="nickname">Card’s number</Label>
                  {countdownActive && (
                    <Label className="float-right">
                      Remaining time: {countdown}
                    </Label>
                  )}
                  <Input
                    onChange={this.onFieldChange}
                    type="text"
                    value={cardNumber}
                    id="cardNumber"
                    placeholder="Enter card’s number"
                  />
                </FormGroup>

                <Button
                  className="float-right"
                  color="primary"
                  type="submit"
                  onClick={this.handleSubmit}
                  disabled={error}
                >
                  Finish
                </Button>
              </form>
            </CardBody>
          </Card>
          <Modal isOpen={this.state.modalVisible}>
            <ModalBody>
              <p>You have registered successfully!</p>
            </ModalBody>
          </Modal>
        </Container>
      </div>
    );
  }
}

export default CardNumberPage;

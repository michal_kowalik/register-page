/*!

=========================================================
* BLK Design System React - v1.2.0
=========================================================

* Product Page: https://www.creative-tim.com/product/blk-design-system-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/blk-design-system-react/blob/main/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import "assets/css/nucleo-icons.css";
import "assets/scss/blk-design-system-react.scss?v=1.2.0";
import "assets/demo/demo.css";

import RegisterPage from "views/RegisterPage/RegisterPage";
import {CARD_NUMBER, REGISTER} from "./const/routes";
import CardNumberPage from "./views/RegisterPage/CardNumberPage/CardNumberPage";

ReactDOM.render(
    <BrowserRouter>
      <Switch>
        <Route path={REGISTER} render={(props) => <RegisterPage {...props} />} />
        <Route
            path={CARD_NUMBER}
            render={(props) => <CardNumberPage {...props} />}
        />
        <Redirect from="/" to="/register-page" />
      </Switch>
    </BrowserRouter>,
  document.getElementById("root")
);
